﻿using System;
using BusinessLogic.BusinessEngines;
using BusinessLogic.BusinessEntities;
using BusinessLogic.Tests.BusinessEngines;
using BusinessLogic.Tests.BusinessEntities;
using Xunit;

namespace BusinessLogic.Tests.BusinessManagers
{
    public class RequestManagerBaseTests
    {
        [Fact]
        public void TestMockRequestManager_InvalidBaseRequest()
        {
            // Here we expect the validation to fail in the ExternalRequestBaseValidator as it
            // for testing purposes has been implemented with a logic that fails if ApplicationId is not above 0
            ExternalRequestBaseValidator baseValidator = new ExternalRequestBaseValidator();
            MockRequestValidator mockRequestValidator = new MockRequestValidator();
            MockRequestManager mockRequestManager = new MockRequestManager(baseValidator, mockRequestValidator);

            MockExternalRequest mockExternalRequest = new MockExternalRequest { ApplicationId = 0 };

            // This will fail on the "base" validation before getting into the actual MockRequestManager
            MockExternalResponse result = mockRequestManager.ProcessRequest(mockExternalRequest);
            Assert.Equal(1, result.ErrorCode);
            Assert.Equal(ErrorLevelType.Error, result.ErrorLevel);
            Assert.Equal("Validation of request failed", result.ErrorMessage);
        }

        [Fact]
        public void TestMockRequestManager_InvalidExternalMockRequest()
        {
            // Here we expect the validation to fail in the MockRequestValidator
            ExternalRequestBaseValidator baseValidator = new ExternalRequestBaseValidator();
            MockRequestValidator mockRequestValidator = new MockRequestValidator();
            MockRequestManager mockRequestManager = new MockRequestManager(baseValidator, mockRequestValidator);

            MockExternalRequest mockExternalRequest = new MockExternalRequest { ApplicationId = 1, MyInputString = "Fail" };

            // This will be valid in the ExternalRequestBaseValidator and then it will fail in MockRequestValidator
            // This will trigger the HandleInvalidRequest inside the MockRequestManager
            MockExternalResponse result = mockRequestManager.ProcessRequest(mockExternalRequest);
            Assert.Equal(2, result.ErrorCode);
            Assert.Equal(ErrorLevelType.Error, result.ErrorLevel);
            Assert.Equal("Invalid MockExternalRequest", result.ErrorMessage);
        }

        [Fact]
        public void TestMockRequestManager_WarningExternalMockRequest()
        {
            // Here we expect the validation to fail in the MockRequestValidator with Warning
            ExternalRequestBaseValidator baseValidator = new ExternalRequestBaseValidator();
            MockRequestValidator mockRequestValidator = new MockRequestValidator();
            MockRequestManager mockRequestManager = new MockRequestManager(baseValidator, mockRequestValidator);

            MockExternalRequest mockExternalRequest = new MockExternalRequest { ApplicationId = 1, MyInputString = "Warning" };

            // This will be valid in the ExternalRequestBaseValidator and then it will fail in MockRequestValidator
            // This will trigger the HandleWarningRequest inside the MockRequestManager
            MockExternalResponse result = mockRequestManager.ProcessRequest(mockExternalRequest);
            Assert.Equal(3, result.ErrorCode);
            Assert.Equal(ErrorLevelType.Warning, result.ErrorLevel);
            Assert.Equal("Warning", result.MyOutputString);
        }

        [Fact]
        public void TestMockRequestManager_ValidExternalMockRequest()
        {
            // Here we expect the validation to succeed in the MockRequestValidator
            ExternalRequestBaseValidator baseValidator = new ExternalRequestBaseValidator();
            MockRequestValidator mockRequestValidator = new MockRequestValidator();
            MockRequestManager mockRequestManager = new MockRequestManager(baseValidator, mockRequestValidator);

            MockExternalRequest mockExternalRequest = new MockExternalRequest { ApplicationId = 1, MyInputString = "Success" };

            // This will be valid in the ExternalRequestBaseValidator and in MockRequestValidator
            // This will trigger the HandleValidRequest inside the MockRequestManager
            MockExternalResponse result = mockRequestManager.ProcessRequest(mockExternalRequest);
            Assert.Equal(0, result.ErrorCode);
            Assert.Equal(ErrorLevelType.None, result.ErrorLevel);
            Assert.Equal("Success", result.MyOutputString);
        }

        [Fact]
        public void TestMockRequestManager_ValidRequest()
        {
            ExternalRequestBaseValidator baseValidator = new ExternalRequestBaseValidator();
            HeavyWorkMockRequestValidator mockRequestValidator = new HeavyWorkMockRequestValidator();
            HeavyValidationMockRequestManager mockRequestManager = new HeavyValidationMockRequestManager(baseValidator, mockRequestValidator);

            MockExternalRequest mockExternalRequest = new MockExternalRequest { MyInputString = "Success", ApplicationId = 1 };

            MockExternalResponse result = mockRequestManager.ProcessRequest(mockExternalRequest);
            Assert.Equal(0, result.ErrorCode);
            Assert.Equal(ErrorLevelType.Info, result.ErrorLevel);
            Assert.Equal("Success", result.MyOutputString);
        }
    }
}
