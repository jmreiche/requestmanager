﻿using BusinessLogic.BusinessEngines;
using BusinessLogic.BusinessEntities;
using BusinessLogic.BusinessEntities.Base;
using BusinessLogic.BusinessManagers;
using BusinessLogic.Tests.BusinessEntities;

namespace BusinessLogic.Tests.BusinessManagers
{
    public class MockRequestManager : RequestManagerBase<MockExternalRequest, MockExternalResponse, InputValidationResult>
    {
        private IRequestValidator<MockExternalRequest> _requestValidator;

        public MockRequestManager(IRequestValidator<RequestBase> baseRequestValidator, IRequestValidator<MockExternalRequest> requestValidator) : base(baseRequestValidator)
        {
            _requestValidator = requestValidator;
        }

        protected override IRequestValidator<MockExternalRequest> GetInputRequestValidator(MockExternalRequest request)
        {
            return _requestValidator;
        }

        protected override MockExternalResponse HandleInvalidRequest(MockExternalRequest request, InputValidationResult failedValidation)
        {
            MockExternalResponse response = new MockExternalResponse
            {
                ErrorCode = 2,
                ErrorMessage = "Invalid MockExternalRequest",
                ErrorLevel = ErrorLevelType.Error
            };

            return response;
        }

        protected override MockExternalResponse HandleValidRequest(MockExternalRequest request, InputValidationResult successfulValidation)
        {
            MockExternalResponse response = new MockExternalResponse
            {
                ErrorCode = 0,
                ErrorLevel = ErrorLevelType.None,
                MyOutputString = "Success"
            };

            return response;
        }

        protected override MockExternalResponse HandleWarningRequest(MockExternalRequest request, InputValidationResult warningValidation)
        {
            MockExternalResponse response = new MockExternalResponse
            {
                ErrorCode = 3,
                ErrorLevel = ErrorLevelType.Warning,
                MyOutputString = "Warning"
            };

            return response;
        }
    }
}
