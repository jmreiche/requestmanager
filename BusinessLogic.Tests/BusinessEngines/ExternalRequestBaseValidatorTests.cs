﻿using System;
using BusinessLogic.BusinessEngines;
using BusinessLogic.BusinessEntities;
using BusinessLogic.BusinessEntities.Base;
using Xunit;

namespace BusinessLogic.Tests.BusinessEngines
{
    public class ExternalRequestBaseValidatorTests
    {
        [Fact]
        public void TestExternalRequestBaseValidator_ValidRequest()
        {
            // Initalize
            ExternalRequestBaseValidator externalRequestBaseValidator = new ExternalRequestBaseValidator();
            // Create valid request (This is simple as we made ApplicationId > 0 equal success
            ExternalRequestBase externalRequest = new ExternalRequestBase() { ApplicationId = 1 };
            // Validate our complicated request
            IInputValidationResult result = externalRequestBaseValidator.Validate(externalRequest);
            // We expect this to be successful
            Assert.Equal(ValidationResultType.Success, result.ValidationResult);
        }

        [Fact]
        public void TestExternalRequestBaseValidator_InvalidRequest()
        {
            // Initalize
            ExternalRequestBaseValidator externalRequestBaseValidator = new ExternalRequestBaseValidator();
            // Create invalid request (This is simple as we made ApplicationId > 0 equal success
            ExternalRequestBase externalRequest = new ExternalRequestBase() { ApplicationId = 0 };
            // Validate our complicated request
            IInputValidationResult result = externalRequestBaseValidator.Validate(externalRequest);
            // We expect this to be error
            Assert.Equal(ValidationResultType.Error, result.ValidationResult);
        }
    }
}
