﻿using BusinessLogic.BusinessEngines;
using BusinessLogic.BusinessEntities;
using BusinessLogic.Tests.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Tests.BusinessEngines
{
    public class MockRequestValidator : IRequestValidator<MockExternalRequest>
    {
        public IInputValidationResult Validate(MockExternalRequest request)
        {
            if (request != null)
            {
                if (request.MyInputString.Equals("Success"))
                    return new InputValidationResult() { ValidationResult = ValidationResultType.Success };
                else if (request.MyInputString.Equals("Warning"))
                    return new InputValidationResult { ValidationResult = ValidationResultType.Warning };
                else
                    return new InputValidationResult() { ValidationResult = ValidationResultType.Error };
            }
            else
                return new InputValidationResult() { ValidationResult = ValidationResultType.Error };
        }
    }
}
